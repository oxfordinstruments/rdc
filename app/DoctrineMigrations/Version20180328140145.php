<?php declare(strict_types = 1);

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180328140145 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('CREATE TEMPORARY TABLE __temp__settings_oir AS SELECT id, uid, password, hub, server, port FROM settings_oir');
        $this->addSql('DROP TABLE settings_oir');
        $this->addSql('CREATE TABLE settings_oir (id INTEGER NOT NULL, uid VARCHAR(20) DEFAULT NULL, password VARCHAR(10) DEFAULT NULL, hub VARCHAR(10) DEFAULT NULL, server VARCHAR(255) DEFAULT NULL, port VARCHAR(255) DEFAULT NULL, enabled BOOLEAN DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('INSERT INTO settings_oir (id, uid, password, hub, server, port) SELECT id, uid, password, hub, server, port FROM __temp__settings_oir');
        $this->addSql('DROP TABLE __temp__settings_oir');
        $this->addSql('CREATE TEMPORARY TABLE __temp__settings_oiv AS SELECT id, enabled FROM settings_oiv');
        $this->addSql('DROP TABLE settings_oiv');
        $this->addSql('CREATE TABLE settings_oiv (id INTEGER NOT NULL, enabled BOOLEAN DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('INSERT INTO settings_oiv (id, enabled) SELECT id, enabled FROM __temp__settings_oiv');
        $this->addSql('DROP TABLE __temp__settings_oiv');
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('CREATE TEMPORARY TABLE __temp__settings_oir AS SELECT id, uid, password, hub, server, port FROM settings_oir');
        $this->addSql('DROP TABLE settings_oir');
        $this->addSql('CREATE TABLE settings_oir (id INTEGER DEFAULT NULL, uid CLOB DEFAULT NULL COLLATE BINARY, password CLOB DEFAULT NULL COLLATE BINARY, hub CLOB DEFAULT NULL COLLATE BINARY, server CLOB DEFAULT NULL COLLATE BINARY, port INTEGER DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('INSERT INTO settings_oir (id, uid, password, hub, server, port) SELECT id, uid, password, hub, server, port FROM __temp__settings_oir');
        $this->addSql('DROP TABLE __temp__settings_oir');
        $this->addSql('CREATE TEMPORARY TABLE __temp__settings_oiv AS SELECT id, enabled FROM settings_oiv');
        $this->addSql('DROP TABLE settings_oiv');
        $this->addSql('CREATE TABLE settings_oiv (id INTEGER DEFAULT NULL, enabled INTEGER DEFAULT 0, PRIMARY KEY(id))');
        $this->addSql('INSERT INTO settings_oiv (id, enabled) SELECT id, enabled FROM __temp__settings_oiv');
        $this->addSql('DROP TABLE __temp__settings_oiv');
    }
}
