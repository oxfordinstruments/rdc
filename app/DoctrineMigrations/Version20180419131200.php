<?php declare(strict_types = 1);

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180419131200 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('ALTER TABLE settings_oir ADD COLUMN console_port VARCHAR(255) DEFAULT NULL');
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('CREATE TEMPORARY TABLE __temp__settings_oir AS SELECT id, uid, password, hub, server, port, account, console_ip, system_type, enabled FROM settings_oir');
        $this->addSql('DROP TABLE settings_oir');
        $this->addSql('CREATE TABLE settings_oir (id INTEGER NOT NULL, uid VARCHAR(20) DEFAULT NULL, password VARCHAR(10) DEFAULT NULL, hub VARCHAR(10) DEFAULT NULL, server VARCHAR(255) DEFAULT NULL, port VARCHAR(255) DEFAULT NULL, account VARCHAR(255) DEFAULT NULL, console_ip VARCHAR(255) DEFAULT NULL, system_type VARCHAR(255) DEFAULT NULL, enabled BOOLEAN DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('INSERT INTO settings_oir (id, uid, password, hub, server, port, account, console_ip, system_type, enabled) SELECT id, uid, password, hub, server, port, account, console_ip, system_type, enabled FROM __temp__settings_oir');
        $this->addSql('DROP TABLE __temp__settings_oir');
    }
}
