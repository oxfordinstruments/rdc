<?php

namespace AppBundle\Form;



use Evotodi\IpFieldTypeBundle\Type\IpType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class NetworkForm extends AbstractType
{
	public function configureOptions(OptionsResolver $resolver)
	{
		$resolver->setDefaults([
		]);
	}

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
	        ->add('dhcp', CheckboxType::class, [
	        	'label' => 'DHCP',
		        'label_attr' => ['class' => 'dhcpLabel']
	        ])
            ->add('addr', IpType::class,[
	            'version' => 'ipv4',
            ])
	        ->add('gw', IpType::class,[
		        'version' => 'ipv4'
	        ])
	        ->add('nm', IpType::class,[
		        'version' => 'ipv4'
	        ])
	        ->add('bcast', IpType::class,[
		        'version' => 'ipv4'
	        ])
	        ->add('ns1', IpType::class,[
		        'version' => 'ipv4'
	        ])
	        ->add('ns2', IpType::class,[
		        'version' => 'ipv4'
	        ])
            ->add('mac', IpType::class, [
            	'version' => 'mac',
	            'readonly' => true
            ])
        ;
    }
}
