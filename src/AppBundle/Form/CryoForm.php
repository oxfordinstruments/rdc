<?php

namespace AppBundle\Form;

use AppBundle\Entity\SettingsOiV;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CryoForm extends AbstractType
{
	public function configureOptions(OptionsResolver $resolver)
	{
		$resolver->setDefaults([
			'data_class' => SettingsOiV::class
		]);
	}

	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
			->add('enabled', CheckboxType::class, [
				'label' => 'Enable OiVision',
				'label_attr' => ['class' => 'enDisLabel']
			]);
	}

	public function getBlockPrefix()
	{
		return 'app_bundle_cryo_form';
	}
}
