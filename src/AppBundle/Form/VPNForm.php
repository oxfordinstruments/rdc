<?php

namespace AppBundle\Form;

use AppBundle\Entity\SettingsOiR;
use Evotodi\IpFieldTypeBundle\Type\IpType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class VPNForm extends AbstractType
{

	public function configureOptions(OptionsResolver $resolver)
	{
		$resolver->setDefaults([
			'data_class' => SettingsOiR::class,
			'system_types' => null
		]);
	}

	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$system_type_choices = [];
		foreach ($options['system_types'] as $id=>$system_type){
			$system_type_choices[$system_type['name']] = $id;
		}

		$builder
			->add('uid', TextType::class,[
				'label' => 'VPN Username',
				'attr' => ['class' => 'vpnEnDis']
			])
			->add('password', TextType::class,[
				'label' => 'VPN Password',
				'attr' => ['class' => 'vpnEnDis']
			])
			->add('hub', TextType::class, [
				'label' => 'VPN Hub',
				'attr' => ['class' => 'vpnEnDis']
			])
			->add('account', TextType::class, [
				'label' => 'VPN Account',
				'attr' => ['class' => 'vpnEnDis vpnAcc'],
				'data' => isset($options['data']) ? is_null($options['data']->getAccount()) ? 'oiremote' : $options['data']->getAccount() : null
			])
			->add('server', TextType::class, [
				'label' => 'VPN Server (optional)',
				'attr' => ['class' => 'vpnEnDis']
			])
			->add('port', IntegerType::class, [
				'label' => 'VPN Port (optional)',
				'attr' => ['class' => 'vpnEnDis']
			])
			->add('console_ip', IpType::class,[
				'label' => 'Console IP',
				'attr' => ['class' => 'vpnEnDis'],
				'version' => 'ipv4'
			])
			->add('console_port', IntegerType::class, [
				'label' => 'Console Port (optional)',
				'attr' => ['class' => 'vpnEnDis']
			])
			->add('system_type', ChoiceType::class, [
				'label' => 'System Type',
				'attr' => ['class' => 'vpnEnDis'],
				'choices' => $system_type_choices,
			])
			->add('enabled', CheckboxType::class, [
				'label' => 'Enable OiRemote',
				'attr' => ['class' => 'vpnEnDisCheck'],
				'label_attr' => ['class' => 'enDisLabel']
			])

		;
	}



	public function getBlockPrefix()
	{
		return 'app_bundle_vpnform';
	}
}
