<?php
/**
 * Created by PhpStorm.
 * User: Justin
 * Date: 3/28/2018
 * Time: 6:53 AM
 */

namespace AppBundle\Entity;

use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(name="settings_oir")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\SettingsOiRRepo")
 */
class SettingsOiR
{
	/**
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 * @ORM\Column(type="integer")
	 */
	private $id;

	/**
	 * @Assert\NotBlank()
	 * @ORM\Column(type="string", length=20, nullable=true)
	 */
	private $uid;

	/**
	 * @Assert\NotBlank()
	 * @ORM\Column(type="string", length=10, nullable=true)
	 */
	private $password;

	/**
	 * @Assert\NotBlank()
	 * @ORM\Column(type="string", length=10, nullable=true)
	 */
	private $hub;

	/**
	 * @ORM\Column(type="string", nullable=true)
	 */
	private $server;

	/**
	 * @ORM\Column(type="string", nullable=true)
	 */
	private $port;

	/**
	 * @ORM\Column(type="string", nullable=true)
	 */
	private $account;

	/**
	 * @ORM\Column(type="string", nullable=true)
	 */
	private $console_ip;

	/**
	 * @ORM\Column(type="string", nullable=true)
	 */
	private $console_port;

	/**
	 * @ORM\Column(type="string", nullable=true)
	 */
	private $system_type;

	/**
	 * @ORM\Column(type="boolean", nullable=true)
	 */
	private $enabled;

	/**
	 * @return integer
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * @return string
	 */
	public function getUid()
	{
		return $this->uid;
	}

	/**
	 * @param string $uid
	 */
	public function setUid($uid)
	{
		$this->uid = $uid;
	}

	/**
	 * @return string
	 */
	public function getPassword()
	{
		return $this->password;
	}

	/**
	 * @param string $password
	 */
	public function setPassword($password)
	{
		$this->password = $password;
	}

	/**
	 * @return string
	 */
	public function getHub()
	{
		return $this->hub;
	}

	/**
	 * @param string $hub
	 */
	public function setHub($hub)
	{
		$this->hub = $hub;
	}

	/**
	 * @return string
	 */
	public function getServer()
	{
		return $this->server;
	}

	/**
	 * @param string $server
	 */
	public function setServer($server)
	{
		$this->server = $server;
	}

	/**
	 * @return integer
	 */
	public function getPort()
	{
		return $this->port;
	}

	/**
	 * @param integer $port
	 */
	public function setPort($port)
	{
		$this->port = $port;
	}

	/**
	 * @return string
	 */
	public function getAccount()
	{
		return $this->account;
	}

	/**
	 * @param string $account
	 */
	public function setAccount($account)
	{
		$this->account = $account;
	}

	/**
	 * @return mixed
	 */
	public function getEnabled()
	{
		return $this->enabled;
	}

	/**
	 * @param mixed $enabled
	 */
	public function setEnabled($enabled)
	{
		$this->enabled = $enabled;
	}

	/**
	 * @return mixed
	 */
	public function getConsoleIp()
	{
		return $this->console_ip;
	}

	/**
	 * @param mixed $console_ip
	 */
	public function setConsoleIp($console_ip)
	{
		$this->console_ip = $console_ip;
	}

	/**
	 * @return mixed
	 */
	public function getSystemType()
	{
		return $this->system_type;
	}

	/**
	 * @param mixed $system_type
	 */
	public function setSystemType($system_type)
	{
		$this->system_type = $system_type;
	}

	/**
	 * @return mixed
	 */
	public function getConsolePort()
	{
		return $this->console_port;
	}

	/**
	 * @param mixed $console_port
	 */
	public function setConsolePort($console_port)
	{
		$this->console_port = $console_port;
	}


}