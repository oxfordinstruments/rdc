<?php
/**
 * Created by PhpStorm.
 * User: Justin
 * Date: 3/28/2018
 * Time: 7:09 AM
 */

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;

class SettingsOiRRepo extends EntityRepository
{
	public function getCurrentSetting()
	{
		try {
			return $this->createQueryBuilder('s')
				->orderBy('s.id')
				->setMaxResults(1)
				->getQuery()
				->getSingleResult();
		} catch (NoResultException $e) {
			return null;
		} catch (NonUniqueResultException $e) {
			return null;
		}
	}

	public function clearTable()
	{
		try{
		return $this->createQueryBuilder('s')
			->delete()
			->getQuery()
			->getSingleScalarResult();
		} catch (NonUniqueResultException $e) {
			return null;
		}
	}
}