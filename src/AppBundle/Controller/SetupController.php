<?php

namespace AppBundle\Controller;

use AppBundle\Entity\SettingsOiR;
use AppBundle\Entity\SettingsOiV;
use AppBundle\Form\CryoForm;
use AppBundle\Form\NetworkForm;
use AppBundle\Form\VPNForm;
use AppBundle\Repository\SettingsOiRRepo;
use AppBundle\Repository\SettingsOiVRepo;
use AppBundle\Resources\ProcessEnhanced;
use AppBundle\Resources\Utilities;
use Exception;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use function Sodium\add;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;
use Symfony\Component\Config\Exception\FileLoaderLoadException;
use Symfony\Component\Console\Output\StreamOutput;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;
use Symfony\Component\Routing\Annotation\Route;
use Net_Ping;
use PEAR;


class SetupController extends Controller
{
	/**
	 * @Route("/setup", name="setup")
	 * @Security("has_role('ROLE_MANAGE')")
	 */
	public function indexAction(Request $request)
	{
		$utils = new Utilities();

		/**
		 * Get RDC Network Info
		 */
		try{
			$process = new Process($this->getParameter('rdc_net_info'));

			$process->run();

			if (!$process->isSuccessful()) {
				throw new ProcessFailedException($process);
			}

			$json = json_decode($process->getOutput());

			$json->eth0->dhcp = false;
			if(file_exists($this->getParameter('rdc_network_dhcp'))){
				$json->eth0->dhcp = true;
			}

			$json->eth0->ns1 = $json->ns[0];
			if(key_exists(1, $json->ns)){
				$json->eth0->ns2 = $json->ns[1];
			}else{
				$json->eth0->ns2 = "...";
			}
		}catch (ProcessFailedException $processFailedException){
			$this->addFlash('danger', $processFailedException->getMessage());
			$json = new \stdClass();
			$json->eth0 = null;
		}catch (\Exception $e){
			$this->addFlash('danger', $e->getMessage());
			$json = new \stdClass();
			$json->eth0 = null;
		}

		try {
			$rtnArr['oirVer'] = file_get_contents($this->getParameter('oir_ver'));
		}catch (Exception $exception){
			$rtnArr['oirVer'] = 'NA';
		}

		try{
			$rtnArr['oivVer'] = file_get_contents($this->getParameter('oiv_ver'));
		}catch (Exception $exception){
			$rtnArr['oivVer'] = 'NA';
		}

		try{
			$rtnArr['oivMac'] = file_get_contents($this->getParameter('oiv_mac'));
		}catch (Exception $exception){
			$this->addFlash("danger", $exception->getMessage());
			$rtnArr['oivMac'] = 'NA';
		}

		$rtnArr['rdcVer'] = $this->getParameter('rdc_ver');

		try{
			$rtnArr['rdcSdVer'] = file_get_contents($this->getParameter('rdc_sd_ver'));
		}catch (Exception $exception){
			$rtnArr['rdcSdVer'] = 'NA';
		}


		/**
		 * @var SettingsOiRRepo $vpnRepo
		 */
		$vpnRepo = $this->getDoctrine()->getRepository(SettingsOiR::class);
		$vpnData = $vpnRepo->getCurrentSetting();

		/**
		 * @var SettingsOiVRepo $oivRepo
		 */
		$oivRepo = $this->getDoctrine()->getRepository(SettingsOiV::class);
		$oivData = $oivRepo->getCurrentSetting();

		$formNet = $this->createForm(NetworkForm::class, $json->eth0);
		$formVpn = $this->createForm(VPNForm::class, $vpnData,['system_types' => $this->getParameter('system_types')]);
		$formCryo = $this->createForm(CryoForm::class, $oivData);

		/**
		 * NET Form
		 */
		$formNet->handleRequest($request);
		if($formNet->isSubmitted() and $formNet->isValid()) {
			$formData = $formNet->getData();
			if($formData->dhcp){
				try{
					$process = new Process($this->getParameter('rdc_network'));
					$process->run();
					if (!$process->isSuccessful()) {
						throw new ProcessFailedException($process);
					}
					$this->addFlash('success', 'RDC network set to DHCP. Please reboot the RDC now.');
				}catch (ProcessFailedException $processFailedException){
					$this->addFlash('danger', $processFailedException->getMessage());
				}catch (\Exception $e){
					$this->addFlash('danger', $e->getMessage());
				}
			}else{
				if(!filter_var($formData->addr, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)){
					$this->addFlash('danger', 'IP Address is invalid');
					$this->redirectToRoute('setup');
				}
				if(!filter_var($formData->nm, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)){
					$this->addFlash('danger', 'Subnet Mask is invalid');
					$this->redirectToRoute('setup');
				}
				if(!filter_var($formData->gw, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)){
					$this->addFlash('danger', 'Gateway Address is invalid');
					$this->redirectToRoute('setup');
				}
				if(!filter_var($formData->bcast, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)){
					$this->addFlash('danger', 'Broadcast Address is invalid');
					$this->redirectToRoute('setup');
				}
				if(!filter_var($formData->ns1, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)){
					$this->addFlash('danger', 'DNS Primary is invalid');
					$this->redirectToRoute('setup');
				}
				if($formData->ns2 != "..."){
					if(!filter_var($formData->ns2, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)){
						$this->addFlash('danger', 'DNS Secondary is invalid');
						$this->redirectToRoute('setup');
					}
				}

				$dns = $formData->ns1;
				if($formData->ns2 != "..."){
					$dns .= " ".$formData->ns2;
				}

				$args = ' "'.$formData->addr.'/'.$utils->mask2cidr($formData->nm).'" "'.$formData->bcast.'" "'.$formData->gw.'" "'.$dns.'"';

				try{
					$process = new Process($this->getParameter('rdc_network').$args);
					$process->run();
					if (!$process->isSuccessful()) {
						throw new ProcessFailedException($process);
					}

				}catch (ProcessFailedException $processFailedException){
					$this->addFlash('danger', $processFailedException->getMessage());
				}catch (\Exception $e){
					$this->addFlash('danger', $e->getMessage());
				}

				$this->addFlash('success', 'RDC network set to static. Please reboot the RDC now and use the new IP address.');
			}
		}


		/**
		 * VPN Form
		 */
		$formVpn->handleRequest($request);
		if($formVpn->isSubmitted() and $formVpn->isValid()) {
			/**
			 * @var SettingsOiR $formData
			 */
			$formData = $formVpn->getData();
			$em = $this->getDoctrine()->getManager();
			$em->persist($formData);
			$em->flush();

			if(!$this->getParameter('windowsOS')){
				if($formData->getEnabled()){
					try{
						touch($this->getParameter('oir_en'));
						if(!file_exists($this->getParameter('oir_en'))){
							throw new Exception("OiRemote did not enable. Could not touch file");
						}
					}catch (\Exception $exception){
						$this->addFlash('danger', $exception->getMessage());
					}

				}else{
					try{
						unlink($this->getParameter('oir_en'));
						if(file_exists($this->getParameter('oir_en'))){
							throw new Exception("OiRemote did not disable. Could not unlink file");
						}
					}catch (\Exception $exception){
						$this->addFlash('danger', $exception->getMessage());
					}
				}
			}else{
				$this->addFlash('info', 'Windows system found! OiRemote ignored!');
			}
			$this->redirectToRoute('setup');
		}


		/**
		 * Cryo Form
		 */
		$formCryo->handleRequest($request);
		if($formCryo->isSubmitted() and $formCryo->isValid()) {
			$formData = $formCryo->getData();
			$em = $this->getDoctrine()->getManager();
			$em->persist($formData);
			$em->flush();

			if(!$this->getParameter('windowsOS')){
				if($formData->getEnabled()){
					try{
						touch($this->getParameter('oiv_en'));
						if(!file_exists($this->getParameter('oiv_en'))){
							throw new Exception("OiVision did not enable. Could not touch file");
						}
					}catch (\Exception $exception){
						$this->addFlash('danger', $exception->getMessage());
					}
				}else{
					try {
						unlink($this->getParameter('oiv_en'));
						if(file_exists($this->getParameter('oiv_en'))){
							throw new Exception("OiVision did not disable. Could not unlink file");
						}
					}catch (\Exception $exception){
						$this->addFlash('danger', $exception->getMessage());
					}
				}
			}else{
				$this->addFlash('info', 'Windows system found! OiVision ignored!');
			}
			$this->redirectToRoute('setup');
		}

		/**
		 * Check Pings
		 */
		$pingWho = $request->query->get('ping');
		if(!is_null($pingWho)){
			$errLevel = error_reporting( E_ALL );
			require_once("Net/Ping.php");
			$ping = Net_Ping::factory();
			if (PEAR::isError($ping)) {
				echo $ping->getMessage();
			} else {
				$ping->setArgs(array('count' => 3));
				$result = $ping->ping($pingWho);
			}

			error_reporting( $errLevel );
			$rtnArr['pingResp'] = implode("\r\n", $result->_raw_data);
			$rtnArr['pingWho'] = $pingWho;
			if(!is_null($result->_target_ip)){
				if($result->_loss >= 25){
					$this->addFlash('info', "Ping response was poor for $pingWho!");
				}else{
					$this->addFlash('success', "Pinging $pingWho was successful!");
				}
			}
		}

		$rtnArr['formNet'] = $formNet->createView();
		$rtnArr['formVpn'] = $formVpn->createView();
		$rtnArr['formCryo'] = $formCryo->createView();
		$rtnArr['vpnAccount'] = $this->getParameter('vpn_default_account');

		return $this->render('main/setup.html.twig', $rtnArr);
	}

	/**
	 * @Route("/setup/viewoivlog", name="viewoivlog")
	 * @Security("has_role('ROLE_MANAGE')")
	 */
	public function viewOiVisionLog(Request $request)
	{
		if (!file_exists($this->getParameter('oiv_log'))) {
			$this->addFlash('danger', 'OiVision Log Not Found!');
			return $this->redirectToRoute('setup');
		}

		return $this->render(':main:log.html.twig', [
			'log' => nl2br( file_get_contents($this->getParameter('oiv_log'))),
			'vpnAccount' => ''
		]);
	}

	/**
	 * @Route("/setup/viewoirlog", name="viewoirlog")
	 * @Security("has_role('ROLE_MANAGE')")
	 */
	public function viewOiRemoteLog(Request $request)
	{
		if (!file_exists($this->getParameter('oir_log'))) {
			$this->addFlash('danger', 'OiRemote Log Not Found!');
			return $this->redirectToRoute('setup');
		}

		return $this->render(':main:log.html.twig', [
			'log' => nl2br( file_get_contents($this->getParameter('oir_log'))),
			'vpnAccount' => ''
		]);
	}

	/**
	 * @Route("/setup/status/oir", name="oir_status")
	 * @Security("has_role('ROLE_MANAGE')")
	 */
	public function oirStatusGet(Request $request)
	{
		if (!$request->isXmlHttpRequest()) {
			throw new BadRequestHttpException('AJAX request expected.');
		}

		if(!file_exists($this->getParameter('oir_en'))){
			$response = new Response("OiRemote is disabled");
		}else{
			$response = new StreamedResponse();
			$response->headers->set('Content-Type', 'text/html');
			$response->headers->set('X-Accel-Buffering', 'no');
			$response->setCallback(function () {
				$process = new ProcessEnhanced('sudo /usr/local/bin/oir_status');
				$process->run(function ($type, $buffer) {
					if (Process::ERR === $type) {
						echo 'ERROR > '.nl2br($buffer);
						ob_flush();
						flush();
					} else {
						echo nl2br($buffer);
						ob_flush();
						flush();
					}
				});
			});
		}

		return $response;
	}

	/**
	 * @Route("/setup/status/oiv", name="oiv_status")
	 * @Security("has_role('ROLE_MANAGE')")
	 */
	public function oivStatusGet(Request $request)
	{
		if(file_exists($this->getParameter('oiv_en'))){
			$status = "OiVision is enabled";
		}else{
			$status = "OiVision is disabled";
		}

		return new Response($status);
	}

	/**
	 * @Method({"GET"})
	 * @Route("/setup/oir/setup", name="oir_setup")
	 * @Security("has_role('ROLE_MANAGE')")
	 */
	public function oir_setup(Request $request)
	{
		if (!$request->isXmlHttpRequest()) {
			throw new BadRequestHttpException('AJAX request expected.');
		}

		if(!file_exists($this->getParameter('oir_en'))){
			$response = new Response("OiRemote is disabled. Setup not allowed!");
		}else{

			$response = new StreamedResponse();
			$response->headers->set('Content-Type', 'text/html');
			$response->headers->set('X-Accel-Buffering', 'no');
			$response->setCallback(function () {
				$repo = $this->getDoctrine()->getRepository(SettingsOiR::class);
				/**
				 * @var SettingsOiR $settings
				 */
				$settings = $repo->getCurrentSetting();
				if(is_null($settings->getAccount())){
					$settings->setAccount($this->getParameter('vpn_default_account'));
				}

				if(is_null($settings->getServer())){
					$args = sprintf(" -u \"%s\" -p \"%s\" -h \"%s\" -a \"%s\"",
						$settings->getUid(),
						$settings->getPassword(),
						$settings->getHub(),
						$settings->getAccount()
					);
				}else{
					$args = sprintf(" -u \"%s\" -p \"%s\" -h \"%s\" -s \"%s:%s\" -a \"%s\"",
						$settings->getUid(),
						$settings->getPassword(),
						$settings->getHub(),
						$settings->getServer(),
						$settings->getPort(),
						$settings->getAccount()
					);
				}
				$process = new ProcessEnhanced('sudo /usr/local/bin/oir_setup'.$args);
				$process->run(function ($type, $buffer) {
					if (Process::ERR === $type) {
						echo 'ERROR > '.nl2br($buffer);
						ob_flush();
						flush();
					} else {
						echo nl2br($buffer);
						ob_flush();
						flush();
					}
				});
			});
		}

		return $response;
	}

	/**
	 * @Method({"GET"})
	 * @Route("/setup/oiv/send", name="oiv_send")
	 * @Security("has_role('ROLE_MANAGE')")
	 */
	public function oiv_send(Request $request)
	{
		if (!$request->isXmlHttpRequest()) {
			throw new BadRequestHttpException('AJAX request expected.');
		}

		if(!file_exists($this->getParameter('oiv_en'))) {
			return new Response("OiVision is disabled. Send not allowed!");
		}

		$response = new StreamedResponse();
		$response->headers->set('Content-Type', 'text/html');
		$response->headers->set('X-Accel-Buffering', 'no');
		$response->setCallback(function () {
			$process = new ProcessEnhanced('sudo oivision -f');
			$process->run(function ($type, $buffer) {
				if (Process::ERR === $type) {
					echo 'ERROR > '.nl2br($buffer);
					ob_flush();
					flush();
				} else {
					echo nl2br($buffer);
					ob_flush();
					flush();
				}
			});
		});

		return $response;
	}
	/**
	 * @Method({"GET"})
	 * @Route("/setup/oir/clear", name="clear_oir_log")
	 * @Security("has_role('ROLE_MANAGE')")
	 */
	public function clear_oir_log(Request $request)
	{
		if (!$request->isXmlHttpRequest()) {
			throw new BadRequestHttpException('AJAX request expected.');
		}

		$response = new StreamedResponse();
		$response->headers->set('Content-Type', 'text/html');
		$response->headers->set('X-Accel-Buffering', 'no');
		$response->setCallback(function () {
			$process = new ProcessEnhanced('sudo /usr/local/bin/clear_oir_log');
			$process->run(function ($type, $buffer) {
				if (Process::ERR === $type) {
					echo 'ERROR > '.nl2br($buffer);
					ob_flush();
					flush();
				} else {
					echo nl2br($buffer);
					ob_flush();
					flush();
				}
			});
		});

		return $response;
	}

	/**
	 * @Method({"GET"})
	 * @Route("/setup/oiv/clear", name="clear_oiv_log")
	 * @Security("has_role('ROLE_MANAGE')")
	 */
	public function clear_oiv_log(Request $request)
	{
		if (!$request->isXmlHttpRequest()) {
			throw new BadRequestHttpException('AJAX request expected.');
		}

		$response = new StreamedResponse();
		$response->headers->set('Content-Type', 'text/html');
		$response->headers->set('X-Accel-Buffering', 'no');
		$response->setCallback(function () {
			$process = new ProcessEnhanced('sudo /usr/local/bin/clear_oiv_log');
			$process->run(function ($type, $buffer) {
				if (Process::ERR === $type) {
					echo 'ERROR > '.nl2br($buffer);
					ob_flush();
					flush();
				} else {
					echo nl2br($buffer);
					ob_flush();
					flush();
				}
			});
		});

		return $response;
	}

	/**
	 * @Method({"GET"})
	 * @Route("/setup/oir/factory", name="oir_factory")
	 * @Security("has_role('ROLE_MANAGE')")
	 */
	public function oir_factory(Request $request)
	{
		if (!$request->isXmlHttpRequest()) {
			throw new BadRequestHttpException('AJAX request expected.');
		}

		$em = $this->getDoctrine()->getRepository(SettingsOiR::class);
		$em->clearTable();
		$response = new StreamedResponse();
		$response->headers->set('Content-Type', 'text/html');
		$response->headers->set('X-Accel-Buffering', 'no');
		$response->setCallback(function () {
			$process = new ProcessEnhanced('sudo /usr/local/bin/oir_factory-defaults quiet');
			$process->run(function ($type, $buffer) {
				if (Process::ERR === $type) {
					echo 'ERROR > '.nl2br($buffer);
					ob_flush();
					flush();
				} else {
					echo nl2br($buffer);
					ob_flush();
					flush();
				}
			});
			echo "Please refresh this page!";
			ob_flush();
			flush();
		});
		$response->send();
		return $response;
	}

	/**
	 * @Method({"GET"})
	 * @Route("/setup/oir/notify", name="oir_notify_server")
	 * @Security("has_role('ROLE_MANAGE')")
	 */
	public function oir_notify_server(Request $request)
	{
		if (!$request->isXmlHttpRequest()) {
			throw new BadRequestHttpException('AJAX request expected.');
		}

		if(!file_exists($this->getParameter('oir_en'))) {
			return new Response("OiRemote is disabled. Notify not allowed!");
		}

		if(!file_exists($this->getParameter('oir_setup_complete'))) {
			return new Response("OiRemote is not setup. Notify not allowed!");
		}

		$response = new StreamedResponse();
		$response->headers->set('Content-Type', 'text/html');
		$response->headers->set('X-Accel-Buffering', 'no');
		$response->setCallback(function () {
			$process = new ProcessEnhanced('sudo /usr/local/bin/notify_server');
			$process->run(function ($type, $buffer) {
				if (Process::ERR === $type) {
					echo 'ERROR > '.nl2br($buffer);
					ob_flush();
					flush();
				} else {
					echo nl2br($buffer);
					ob_flush();
					flush();
				}
			});
		});

		return $response;
	}

	/**
	 * @Method({"GET"})
	 * @Route("/setup/oiv/factory", name="oiv_factory")
	 * @Security("has_role('ROLE_MANAGE')")
	 */
	public function oiv_factory(Request $request)
	{
		if (!$request->isXmlHttpRequest()) {
			throw new BadRequestHttpException('AJAX request expected.');
		}

		$em = $this->getDoctrine()->getRepository(SettingsOiV::class);
		$em->clearTable();

		$response = new StreamedResponse();
		$response->headers->set('Content-Type', 'text/html');
		$response->headers->set('X-Accel-Buffering', 'no');
		$response->setCallback(function () {
			$process = new ProcessEnhanced('sudo /usr/local/bin/oivision -r');
			$process->run(function ($type, $buffer) {
				if (Process::ERR === $type) {
					echo 'ERROR > '.nl2br($buffer);
					ob_flush();
					flush();
				} else {
					echo nl2br($buffer);
					ob_flush();
					flush();
				}
			});
			echo "Please refresh this page!";
			ob_flush();
			flush();
		});

		return $response;
	}

	/**
	 * @Method({"GET"})
	 * @Route("/setup/reboot", name="reboot")
	 * @Security("has_role('ROLE_MANAGE')")
	 */
	public function reboot(Request $request)
	{
		if (!$request->isXmlHttpRequest()) {
			throw new BadRequestHttpException('AJAX request expected.');
		}

		$em = $this->getDoctrine()->getRepository(SettingsOiV::class);
		$em->clearTable();

		$response = new StreamedResponse();
		$response->headers->set('Content-Type', 'text/html');
		$response->headers->set('X-Accel-Buffering', 'no');
		$response->setCallback(function () {
			$process = new ProcessEnhanced('sudo rdc_reboot');
			$process->run(function ($type, $buffer) {
				if (Process::ERR === $type) {
					echo 'ERROR > '.nl2br($buffer);
					ob_flush();
					flush();
				} else {
					echo nl2br($buffer);
					ob_flush();
					flush();
				}
			});
			echo "Please refresh this page!";
			ob_flush();
			flush();
		});

		return $response;
	}

	/**
	 * @Method({"GET"})
	 * @Route("/setup", name="refresh")
	 * @Security("has_role('ROLE_MANAGE')")
	 */
	public function refresh(Request $request)
	{
		if (!$request->isXmlHttpRequest()) {
			throw new BadRequestHttpException('AJAX request expected.');
		}
		return $this->redirectToRoute('setup');
	}

	/**
	 *
	 * This funct is here just for testing the OiRemote notify_server script
	 *
	 * @Route("/remote/update", name="test_update")
	 */
	public function test(Request $request)
	{
		$odata = [
			'code' => 202,
			'error' => false,
		];

		$idata = false;
		try{
			$idata = json_decode($request->getContent());
			if(is_null($idata)){
				throw new Exception('Data is null');
			}
		}
		catch (Exception $e){
			$odata['code'] = 400;
			$odata['error'] = $e->getMessage();
		}

		return new JsonResponse($odata, $odata['code']);
	}

	/**
	 * @Method({"GET"})
	 * @Route("/test/test", name="test")
	 * @Security("has_role('ROLE_MANAGE')")
	 */
	public function test2(Request $request)
	{
//		while(@ob_end_clean());
//		ob_implicit_flush(true);
//		if (!$request->isXmlHttpRequest()) {
//			throw new BadRequestHttpException('AJAX request expected.');
//		}

		$response = new StreamedResponse();
		$response->headers->set('Content-Type', 'text/html');
		$response->headers->set('X-Accel-Buffering', 'no');
		$response->setCallback(function () {
			$process = new ProcessEnhanced('ajaxtest');
			$process->enableOutput();
			$process->run(function ($type, $buffer) {
				if (Process::ERR === $type) {
					echo 'ERROR > '.nl2br($buffer);
					ob_flush();
					flush();
				} else {
					echo nl2br($buffer);
					ob_flush();
					flush();
				}
			});
			echo "Please refresh this page!";
			ob_flush();
			flush();
//			echo "hello";
//			ob_flush();
//			flush();
//			sleep(1);
//			echo "hello";
//			ob_flush();
//			flush();
//			sleep(1);
//			echo "hello";
//			ob_flush();
//			flush();
//			sleep(1);
//			echo "hello";
//			ob_flush();
//			flush();

		});

		return $response;
	}
}
