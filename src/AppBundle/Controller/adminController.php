<?php

namespace AppBundle\Controller;

use AppBundle\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

class adminController extends Controller
{
	/**
	 * @Route("/admin", name="admin")
	 * @Security("has_role('ROLE_ADMIN')")
	 */
	public function indexAction()
	{
		return $this->render('main/admin.html.twig',[
			'vpnAccount' => $this->getParameter('vpn_default_account')
		]);
	}
}
