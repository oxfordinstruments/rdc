<?php

namespace AppBundle\Controller;

use AppBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

class MainController extends Controller
{
	/**
	 * @Route("/", name="homepage")
	 */
    public function homepageAction()
    {
    	if($this->getUser()){
		    /**
		     * @var User $user
		     */
    		$user = $this->getUser();
    		if($user->getUsername() == 'admin'){
			    return $this->redirectToRoute('admin');
		    }

    		return $this->redirectToRoute('setup');
	    }

        return $this->render('main/homepage.html.twig', [
        	'vpnAccount' => ''
        ]);
    }
}