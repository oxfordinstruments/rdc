<?php
/**
 * Created by PhpStorm.
 * User: Justin
 * Date: 4/10/2018
 * Time: 12:24 PM
 */

namespace AppBundle\Resources;


class Utilities
{
	public function mask2cidr($mask)
	{
		$long = ip2long($mask);
		$base = ip2long('255.255.255.255');
		return intval(32 - log(($long ^ $base) + 1, 2));
	}

	public function nl2br($string)
	{

	}
}