<?php
/**
 * Created by PhpStorm.
 * User: Justin
 * Date: 3/28/2018
 * Time: 1:08 PM
 */

namespace AppBundle\Resources;


use Symfony\Component\Process\Process;

/**
 * Temporary class to ensure that symfony processes
 * never run out of memory.
 *
 * @todo: Revert back to core Symfony if/when core Process
 * is fixed.
 */
class ProcessEnhanced extends Process
{
	protected function buildCallback(callable $callback = NULL)
	{
		$out = self::OUT;
		$callback = function ($type, $data) use ($callback, $out) {
			if (null !== $callback) {
				call_user_func($callback, $type, $data);
			} else {
				if ($out == $type) {
					$this->addOutput($data);
				} else {
					$this->addErrorOutput($data);
				}
			}
		};
		return $callback;
	}
}