var vpnAccount = null;
document.addEventListener('DOMContentLoaded', function() {
	var jsData = document.querySelector('#jsData');
	vpnAccount = jsData.dataset.defaultVpnAccount;
});

+function ($) {

    $(document).ready(function() {
        $('.js-header-search-toggle').on('click', function() {
            $('.search-bar').slideToggle();
        });

        $("#dhcp").change(function () {
	        if(this.checked){
		        $("#static input").each(function () {
			        $(this).prop('readonly', true);
			        $(this).addClass('macDis');
		        })
	        }else{
		        $("#static input").each(function () {
			        $(this).prop('readonly', false);
			        $(this).removeClass('macDis');
		        })
	        }
        });

	    if( $("#dhcp").is(':checked') ){
		    $("#static input").each(function () {
			    $(this).prop('readonly', true);
			    $(this).addClass('macDis');
		    })
	    }else{
		    $("#static input").each(function () {
			    $(this).prop('readonly', false);
			    $(this).removeClass('macDis');
		    })
	    }

	    $(".vpnEnDisCheck").change(function () {
	    	console.log(this);
		    if(this.checked){
			    $(".vpnEnDis").each(function () {
				    $(this).prop('readonly', false);
			    })
			    if($(".vpnAcc").val() == ""){
					$(".vpnAcc").val(vpnAccount);
			    }
		    }else{
			    $(".vpnEnDis").each(function () {
				    $(this).prop('readonly', true);
			    })
		    }
	    });

	    if( $(".vpnEnDisCheck").is(':checked') ){
		    $("vpnEnDis").each(function () {
			    $(this).prop('readonly', false);
		    })
		    if($(".vpnAcc").val() == ""){
			    $(".vpnAcc").val(vpnAccount);
		    }
	    }else{
		    $(".vpnEnDis").each(function () {
			    $(this).prop('readonly', true);
		    })
	    }

	    autosize($('textarea'));

	    $(".alert-dismissible").fadeTo(10000, 500).slideUp(500, function(){
		    $(".alert-dismissible").alert('close');
	    });

    });

}(jQuery);

function showAlert(message, type, closeDelay) {
	// default to alert-info; other options include success, warning, danger
	type = type || "info";

	// create the alert div
	var alert = $('<div class="alert alert-' + type + ' alert-dismissible" role="alert">')
		.append(
			$('<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>')
		)
		.append(message);

	// add the alert div to top of alerts-container, use append() to add to bottom
	$("#messages").append(alert);

}