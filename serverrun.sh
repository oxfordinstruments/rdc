#!/usr/bin/env bash

if [ "$SYMFONY_ENV" = "prod" ]; then
php bin/console cache:clear --env=prod
fi
php -v
php bin/console server:run 192.168.14.151:8000