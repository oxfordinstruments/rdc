#!/usr/bin/env bash
php bin/console doctrine:migrations:diff
if [ "$1" != "quiet" ]; then
echo "Hit any key to continue..."
read NONE
fi